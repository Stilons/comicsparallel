import autoHeight from './modules/autoHeight';
import lazyLoad from './modules/lazyLoad';

import accordion from './modules/accordion'
import modal from './modules/modal'
import phoneMask from './modules/phoneMask'
import sliders from './modules/sliders'
import lightbox from './modules/lightbox'
import card from './modules/card'
import customOrder from './modules/customOrder'
import flyCart from './modules/flycart'
import mobileSide from './modules/mobileSide'
import order from './modules/order'
import tabs from './modules/tabs'

require('./scripts');