import {visibleCart} from "./flycart";
import {visibleModal} from "./modal";
import {visibleMobileMenu} from "./mobileSide";

export function visibleOverlay(visible = 'toggle') {
    let overlay = document.querySelector('.js-overlay');
    switch (visible) {
        case 'show':
            overlay.classList.add('_active');
            break;
        case 'hide':
            overlay.classList.remove('_active');
            break;
        default:
            overlay.classList.toggle('_active');
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let overlay = document.querySelector('.js-overlay');
    overlay.addEventListener('click', ()=> {
        visibleCart('hide');
        visibleModal('hide');
        visibleOverlay('hide');
        visibleMobileMenu('hide');
    });
});
