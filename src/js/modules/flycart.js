import {visibleOverlay} from "./overlay";

export function visibleCart(visible = 'toggle') {
    let flyCart = document.querySelector('.js-fly-cart');

    switch (visible) {
        case 'show':
            flyCart.classList.add('_active');
            break;
        case 'hide':
            flyCart.classList.remove('_active');
            break;
        default:
            flyCart.classList.toggle('_active');
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let flyCartBtn = document.querySelector('.js-fly-cart-btn');

    if (flyCartBtn) {
        flyCartBtn.addEventListener('click', ()=> {
            visibleCart();
            visibleOverlay();
        })
    }
});