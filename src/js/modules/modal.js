import {visibleOverlay} from "./overlay";

//Управление видимостью модалок
export function visibleModal(visible = 'toggle') {
    let modals = document.querySelectorAll('.auth-modal');
    document.body.style.overflow = 'initial';
    switch (visible) {
        case 'show':
            modals.forEach((item) => {
                item.classList.add('_active');
            });
            break;
        case 'hide':
            modals.forEach((item) => {
                item.classList.remove('_active');
            });
            break;
        default:
            modals.forEach((item) => {
                item.classList.toggle('_active');
            });
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let modalsLink = document.querySelectorAll("[data-type='modal']"),
        modalClose = document.querySelectorAll(".js-close");

    //Открытие модалки
    modalsLink.forEach((item) => {
        item.addEventListener("click", function (e) {
            e.preventDefault();
            visibleModal('hide');
            let modalName = item.getAttribute('data-modal-link'),
                modalItem = document.querySelector(`[data-modal='${modalName}']`);
            modalItem.classList.add('_active');
            document.body.style.overflow = 'hidden';
            visibleOverlay('show');
        });
    });

    // Закрытие при клике на кнопку закрытия
    modalClose.forEach((item) => {
        item.addEventListener("click", ()=> {
            e.preventDefault();
            visibleModal('hide');
            visibleOverlay('hide');
        });
    });
});