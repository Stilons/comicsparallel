document.addEventListener('DOMContentLoaded', () => {
    let orderItem = document.querySelectorAll('.js-order');
    orderItem.forEach(item => {
        item.addEventListener('click', ()=> {
            item.classList.add('_active');
            item.closest('.current-order').querySelector('.js-order-body').classList.toggle('_active');
        });
    });
})