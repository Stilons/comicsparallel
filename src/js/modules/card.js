document.addEventListener('DOMContentLoaded', ()=> {
    let showMoreCastBtn = document.querySelector('.js-cast-more'),
        showMoreCast = document.querySelector('.js-cast-all-cast');

    // Максимальная высота блока
    const showMoreCastMinHeight = '112px';

    // Устанавливаем максимальную высоту блока при инициализации
    if(showMoreCast) {
        showMoreCast.style.maxHeight = showMoreCastMinHeight;

        // Смена текста и высоты блока при клике на кнопку «Показать/Скрыть»
        showMoreCastBtn.addEventListener('click', ()=> {
            showMoreCast.classList.toggle('_hide');
            if (showMoreCast.classList.contains('_hide')) {
                showMoreCastBtn.textContent = 'Показать больше';
                showMoreCast.style.maxHeight = showMoreCastMinHeight;
            } else {
                showMoreCastBtn.textContent = 'Скрыть';
                showMoreCast.style.maxHeight = showMoreCast.firstElementChild.getBoundingClientRect().height + 'px';
            }
        });
    }
})