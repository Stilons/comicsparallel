document.addEventListener("DOMContentLoaded", function () {
    let tabs = document.querySelector('.js-order-tabs-btn'),
        tabsBtns = tabs.querySelectorAll("[data-type='tab-btn']"),
        tabsContent = tabs.querySelectorAll(".tabs__content [data-type='tab-content']");

    //Скрыть все табы
    function hideTabs() {
        Array.from(tabsBtns).forEach((item) => {
            item.classList.remove('_active');
        });
        Array.from(tabsContent).forEach((item) => {
            item.classList.remove('_active');
        });
    }

    //Показать нужный таб
    function showTab(btn) {
        hideTabs();
        btn.classList.add('_active');
        let tabContent = tabs.querySelector(`[data-tab-item='${btn.getAttribute('data-tab-link')}']`);
        tabContent.classList.add('_active');
    }

    //При клике находить нужный таб
    Array.from(tabsBtns).forEach((item) => {
        item.addEventListener("click", function (e) {
            e.preventDefault();
            showTab(item);
        });
    });

    //Скрываем все табы и показываем первый
    hideTabs();
    showTab(tabsBtns[0]);
});