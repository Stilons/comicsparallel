import Swiper, { Navigation, Pagination, Thumbs } from 'swiper';
Swiper.use([Navigation, Pagination, Thumbs]);

document.addEventListener('DOMContentLoaded', () => {
    if(typeof Swiper !== 'undefined') {
        let swiper = new Swiper(".js-slider-thumbs", {
            loop: true,
            spaceBetween: 16,
            slidesPerView: 4,
            direction: 'vertical',
            watchSlidesProgress: true,
        });
        let swiper2 = new Swiper(".js-slider-main", {
            loop: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            thumbs: {
                swiper: swiper,
            },
        });
    }
});