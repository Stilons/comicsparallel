import {visibleOverlay} from "./overlay";

// Управление видимостью мобильного меню
export function visibleMobileMenu(visible = 'toggle') {
    let mobileMenuBtn = document.querySelector('.js-mobile-btn'),
        mobileMenu = document.querySelector('.js-mobile-menu');

    switch (visible) {
        case 'show':
            mobileMenu.classList.remove('hidden');
            mobileMenuBtn.classList.add('active');
            document.body.classList.add('_no-scroll');
            break;
        case 'hide':
            mobileMenu.classList.add('hidden');
            mobileMenuBtn.classList.remove('active');
            document.body.classList.remove('_no-scroll');
            break;
        default:
            mobileMenu.classList.toggle('hidden');
            mobileMenuBtn.classList.toggle('active');
            document.body.classList.toggle('_no-scroll');
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let mobileMenuBtn = document.querySelector('.js-mobile-btn'),
        mobileMenuLinks = document.querySelectorAll('.js-mobile-menu a');

    mobileMenuBtn.addEventListener('click', e => {
        e.preventDefault();
        visibleMobileMenu();
        visibleOverlay();
    });

    mobileMenuLinks.forEach(mobileMenuLinks => {
        mobileMenuLinks.addEventListener('click',  ()=> {
            visibleMobileMenu('hide');
            visibleOverlay('hide');
        });
    })
});
