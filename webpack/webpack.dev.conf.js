const path = require('path');
const { merge } = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devWebpackConfig = merge(baseWebpackConfig, {
    mode: 'development',
    devServer: {
        static: {
            directory: path.join(__dirname, '../public'),
        },
        compress: true,
        hot: true,
        port: 9000,
        client: {
            overlay: true,
        },
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/twig/pages/index.twig',
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/catalog.twig',
            filename: 'catalog.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/card.twig',
            filename: 'card.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/contacts.twig',
            filename: 'contacts.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/marvel-favorite.twig',
            filename: 'marvel-favorite.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/lk.twig',
            filename: 'lk.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/favorites.twig',
            filename: 'favorites.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/history.twig',
            filename: 'history.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/order.twig',
            filename: 'order.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/success.twig',
            filename: 'success.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/faq.twig',
            filename: 'faq.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/about.twig',
            filename: 'about.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/delivery.twig',
            filename: 'delivery.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/revert.twig',
            filename: 'revert.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/politics.twig',
            filename: 'politics.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/twig/pages/404.twig',
            filename: '404.html'
        }),
    ]
});

module.exports = new Promise((resolve, reject) => {
    resolve(devWebpackConfig)
});

